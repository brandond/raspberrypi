/*
*  Basic LED controller app. Mimics street light with LEDs
*  20-May-2017
*  Brandon Davies
*/

#include <stdio.h>

//prototypes for gpioControll library
void setup_io();
void setPin(int pin);
void clearPin(int pin);
void initPin(int pin);

//pin defines for LEDs
#define RED_LED 7
#define YELLOW_LED 8
#define GREEN_LED 11

//Time on for each led
#define RED_LIGHT_LENGTH_SECONDS 5
#define YELLOW_LIGHT_LENGTH_SECONDS 2
#define GREEN_LIGHT_LENGTH_SECONDS 5

int main(int argc, char **argv)
{
	int g,rep;
	printf("Start\n");

	//initilize gpioController
	setup_gpio();
 	
	//set LED pins to output
	setPinGPIOOut(RED_LED);
	setPinGPIOOut(YELLOW_LED);
	setPinGPIOOut(GREEN_LED);
 
	//initilize lights
	clearPin(RED_LED);
	clearPin(YELLOW_LED);
	clearPin(GREEN_LED);

	while(1){
		//run rted light
		setPin(RED_LED);
		sleep(RED_LIGHT_LENGTH_SECONDS);
		clearPin(RED_LED);
		//turn off red light

		//run green light
		setPin(GREEN_LED);
		sleep(GREEN_LIGHT_LENGTH_SECONDS);
		clearPin(GREEN_LED);
		//turn off green light

		//run yellow light
		setPin(YELLOW_LED);
		sleep(YELLOW_LIGHT_LENGTH_SECONDS);
		clearPin(YELLOW_LED);
		//turn off yellow light
	}

  	return 0;
} 


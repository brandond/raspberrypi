/*
*  Basic hal for a raspberryPi
*  Can be used for basic GPIO output
*  Currently only has basic error checking
*  01-May-2017
*  Brandon Davies
*/

#define BCM2837_PERI_BASE        	0x3F000000
#define GPIO_BASE               		(BCM2837_PERI_BASE + 0x200000)
 #define BLOCK_SIZE (4*1024)

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>
 
volatile unsigned *gpio;
int initilized = 0;
 
/* Function:		setup_gpio
*   Description:	gets the memory handle to modify the registers to control the GPIO
*   Parameters:	void
*   Return:		int error: 0 is ok. < 0 is error
*/
int setup_gpio(void){
	int error = 0;
	void *gpio_map;
	int  mem_fd;

	if ((mem_fd = open("/dev/mem", O_RDWR|O_SYNC) ) < 0) {
		error = -1;
	}
 	else{
		/* mmap GPIO */
		gpio_map = mmap(
			NULL,             			//Any adddress in our space will do
			BLOCK_SIZE,       			//Map length
			PROT_READ|PROT_WRITE,// Enable reading & writting to mapped memory
			MAP_SHARED,       		//Shared with other processes
			mem_fd,           			//File to map
			GPIO_BASE         			//Offset to GPIO peripheral
		);
	 
		close(mem_fd); //No need to keep mem_fd open after mmap
	 
		if (gpio_map == MAP_FAILED) {
			error = -2;
		}
		 else{
			gpio = (volatile unsigned *)gpio_map;
		}
 	}
	if(error == 0){
		initilized = 1;
	}
	return error;
}

/* Function:		setPin
*   Description:	sets a pins state to on
*   Parameters:	int pin:	pin to be set. can be between 0 and 60
*   Return:		int error: 0 is ok. < 0 is error
*/
int setPin(int pin){
	int error = 0;
	if(initilized == 0){
		//the setup_gpio function was not run, or failed
		error = -1;
	}
	else if(pin < 32){
		*(gpio+(0x1C/4)) = 1<<pin;
	}
	else if(pin < 60){
		*(gpio+(0x20/4)) = 1<<pin;
	}
	else{
		//pin passed in is not valid
		error = -2;
	}
	return error;
}

/* Function:		clearPin
*   Description:	clears a pins state setting the pin to off
*   Parameters:	int pin:	pin to be cleared. can be between 0 and 60
*   Return:		int error: 0 is ok. < 0 is error
*/
int clearPin(int pin){
	int error = 0;
	if(initilized == 0){
		//the setup_gpio function was not run, or failed
		error = -1;
	}
	else if(pin < 32){
		*(gpio+(0x28/4)) = 1<<pin;
	}
	else if(pin < 60){
		*(gpio+(0x2C/4)) = 1<<pin;
	}
	else{
		//pin passed in is not valid
		error = -2;
	}
	return error;
}

/* Function:		setPinGPIOOut
*   Description:	set a pin to be a GPIO output
*   Parameters:	int pin:	pin to be cleared. can be between 0 and 60
*   Return:		int error: 0 is ok. < 0 is error
*/
int setPinGPIOOut(int pin){
	int error = 0;
	if(initilized == 0){
		//the setup_gpio function was not run, or failed
		error = -1;
	}
	else if(pin < 60){
		//clear GPIO pin settings
		*(gpio+((pin)/10)) &= ~(7<<(((pin)%10)*3));
		//set GPIO pin to output
		*(gpio+((pin)/10)) |=  (1<<(((pin)%10)*3));
	}
	else{
		//pin passed in is not valid
		error = -2;
	}
	return error;
}
